/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var ROWS = 50;
var COLS = 50;

Cell = function () {
    this.alive = false;
}

Game = function (view) {
    this.init = function (cols, rows) {
        this.cells = new Array(cols);
        for (x = 0; x < cols; x++) {
            this.cells[x] = new Array(rows);
            for (y = 0; y < rows; y++) {
                this.cells[x][y] = new Cell();
            }
        }
        view.init(COLS, ROWS);
    }
    this.update = function () {
        var cellsToKill = [];
        var cellsToSpawn = [];
        for (x = 0; x < COLS; x++) {
            for (y = 0; y < ROWS; y++) {
                var myneighbours = this.numOfNeighbours(x, y);
                if (myneighbours < 2 || myneighbours > 3) {
                    cellsToKill.push(this.cells[x][y]);
                    view.updateCell(x, y, false);
                } else {
                    if (!this.cells[x][y].alive && myneighbours == 3) {
                        cellsToSpawn.push(this.cells[x][y]);
                        view.updateCell(x, y, true);
                    }
                }
            }
        }
        for (i = 0; i< cellsToKill.length; i++) {
            cellsToKill[i].alive = false;
        }
        for (i = 0; i< cellsToSpawn.length; i++) {
            cellsToSpawn[i].alive = true;
        }
    }
    
    this.toggleCellState = function (x, y) {
        this.cells[x][y].alive = !this.cells[x][y].alive;
        view.updateCell(x,y,this.cells[x][y].alive);
    }
    
    
    this.getCell = function (x, y) {
        if (x < 0 || y < 0)
            return false;
        if (x >= COLS || y >= ROWS)
            return false;
        return this.cells[x][y];
    }

    this.numOfNeighbours = function (x, y) {
        var num = 0;
        var neighbours = [[x - 1, y - 1], [x, y - 1], [x + 1, y - 1], [x - 1, y], [x + 1, y], [x - 1, y + 1], [x, y + 1], [x + 1, y + 1]];
        for (i = 0; i < neighbours.length; i++) {
            if (this.getCell(neighbours[i][0], neighbours[i][1]).alive) {
                num++;
            }
        }
        return num;
    }

}

View = function () {
    this.init = function (cols, rows) {
        this.container = document.getElementById("container");
        this.cellElements = new Array(cols);
        for (x = 0; x < cols; x++) {
            this.cellElements[x] = new Array(rows);
            for (y = 0; y < rows; y++) {
                this.cellElements[x][y] = document.createElement("span");
                this.cellElements[x][y].setAttribute("class", "cell");
                this.cellElements[x][y].setAttribute("onclick", "game.toggleCellState("+x+","+y+")");
                this.container.appendChild(this.cellElements[x][y]);
            }
        }
    }

    this.updateCell = function (x, y, state) {
        if (state) {
            this.cellElements[x][y].setAttribute("class", "cell alive");
        }
        else {
            this.cellElements[x][y].setAttribute("class", "cell");
        }
    }
}